# README #

This repository contains my PhD template as I've used it in 2012.
It is based on the Austrian outlines at Technical University Graz.

### What is this repository for? ###

* congrats for making it so far!
* PhD_main.pdf is the thesis file
* check chapter_bacon.tex for lorem bacon examples of layout and quotes
* papers/abbreviation_long.bib for proper bib references
* make sure you ack who helper you get there! 

### Who do I talk to? ###

* Hayko Riemenschneider 
* Template bacon example: [thesis2012_phd_riemenschneider_template.pdf](http://www.vision.ee.ethz.ch/~rhayko/paper/thesis2012_phd_riemenschneider_template.pdf)
* Actual thesis: [thesis2012_phd_riemenschneider.pdf](http://www.vision.ee.ethz.ch/~rhayko/paper/thesis2012_phd_riemenschneider.pdf)